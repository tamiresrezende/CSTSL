# Classificador de Séries Temporais de Sinais de Libras

*Paper: Título.* [[Link]()]

# Autores:
Tamires Martins Rezende, Patrícia de Oliveira e Lucas, Sílvia Grasiella Moreira Almeida, Frederico Gadelha Guimarães.


# Descrição do Projeto:

Descrição do Projeto...


# References
- https://arxiv.org/pdf/1803.01271 (An Empirical Evaluation of Generic Convolutional and Recurrent Networks
for Sequence Modeling)
- https://github.com/philipperemy/keras-tcn (GitHub repository)


## Citation

```
@misc{STSL,
  author = {Tamires Rezende, Patrícia Lucas},
  title = {Classificador de Séries Temporais de Sinais de Libras},
  year = {2021},
  publisher = {GitHub},
  journal = {GitHub repository},
  howpublished = {\url{https://gitlab.com/tamiresrezende/CSTSL}},
}
```
